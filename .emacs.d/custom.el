(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(safe-local-variable-values
   '((TeX-master . "single")
     (TeX-master . "space")
     (ispell-personal-dictionary . ".hunspell_space")
     (ispell-personal-dictionary . "~/.hunspell_space")
     (ispell-personal-dictionary "~/.hunspell_space")
     (eval font-latex-add-keywords
           '(("say" "{")
             ("think" "{"))
           'italic-command)
     (eval font-latex-add-keywords
           '(("nothing" "{"))
           'function)
     (eval font-latex-add-keywords
           '(("nothing" "{"))
           'font-latex-string-face)
     (eval font-latex-add-keywords
           '(("nothing" "{"))
           'italic-command))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
