
;;; init.el -*- lexical-binding: t; -*-

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

(setq hunspell-program "hunspell")
(setq pdf-viewer-program "okular")
(setq zsh-programm "zsh")
(setq eglot-exe-path "")

(when (eq system-type 'windows-nt)
  (message "Windows detected.")
  (setq w32-allow-system-shell t)
  (setq save-interprogram-paste-before-kill 1)
  (setq find-program "D:/msys64/usr/bin/find.exe")
  (setq hunspell-program "D:/msys64/mingw64/bin/hunspell.exe")
  (setq pdf-viewer-program "SumatraPDF-3.2-64")
  (setq zsh-programm "D:/msys64/usr/bin/zsh.exe")
  (setq eglot-exe-path "d:/msys64/mingw64/bin"))

(set-frame-size (selected-frame) 130 35)
;;(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 10)
(winner-mode 1)

(setq inhibit-startup-screen t
      initial-scratch-message nil
      sentence-end-double-space nil
      visible-bell t
      ring-bell-function 'ignore
      frame-resize-pixelwise t
      delete-by-moving-to-trash t)

;; always allow 'y' instead of 'yes'.
(defalias 'yes-or-no-p 'y-or-n-p)

;; write over selected text on input... like all modern editors do
(delete-selection-mode t)

;; stop emacs from littering the file system with backup files
(setq make-backup-files nil
      auto-save-default nil
      create-lockfiles nil)

;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;; Do not saves duplicates in kill-ring
(setq kill-do-not-save-duplicates t)

;; Make scrolling less stuttered
(setq auto-window-vscroll nil)
(setq fast-but-imprecise-scrolling t)
(setq scroll-conservatively 101)
(setq scroll-margin 0)
(setq scroll-preserve-screen-position t)

(global-visual-line-mode t)
(global-hl-line-mode t)

(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)

(put 'upcase-region 'disabled nil)         ; C-x C-u
(put 'downcase-region 'disabled nil)       ; C-x C-l

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "M-z") 'zap-up-to-char)
(global-set-key (kbd "<f5>") 'revert-buffer)
(global-set-key (kbd "C-z") 'undo)
(global-set-key (kbd "C-M-<backspace>") 'kill-whole-line)
(global-set-key (kbd "M-j") 'join-line)
(global-set-key (kbd "<f7>") 'compile)

(setq display-time-24hr-format t)
(display-time-mode 1)

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq require-final-newline t)

;; Install straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


(straight-use-package 'use-package)

;; Add frame borders and window dividers
(modify-all-frames-parameters
 '((right-divider-width . 15)
   (internal-border-width . 15)))
(dolist (face '(window-divider
                window-divider-first-pixel
                window-divider-last-pixel))
  (face-spec-reset-face face)
  (set-face-foreground face (face-attribute 'default :background)))


;; all-the-icons-install-fonts
(use-package all-the-icons
  :straight t
  :custom
  (all-the-color-icons t))

(use-package doom-themes
  :straight t)
(load-theme 'doom-one t)

(use-package doom-modeline
  :straight t
  :init
  ;; Start up the modeline after initialization is finished
  :hook (after-init . doom-modeline-mode)
  :custom (doom-modeline-height 30))

;; M-x nerd-icons-install-fonts
(setq doom-modeline-icon t)

(defun efs/set-font-faces ()
  ;; Set the default font face
  (set-face-attribute 'default nil :font "Iosevka" :height 120)
  ;; Set the fixed pitch face
 ;; (set-face-attribute 'fixed-pitch nil :font "Iosevka Comfy Fixed" :height 120)
  ;; Set the variable pitch face
  (set-face-attribute 'variable-pitch nil :font "Iosevka Aile" :height 120)
  )

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (setq doom-modeline-icon t)
                (with-selected-frame frame
                  (efs/set-font-faces))))
  (efs/set-font-faces))

(setq-default line-spacing 5)
(column-number-mode)
(global-display-line-numbers-mode t)

(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Use riggrep instead of grep
(use-package rg
    :straight (rg :type git :host github :repo "dajva/rg.el")
    :config
    (rg-enable-default-bindings)
    (rg-enable-menu))

;; Vertico & Friends
(use-package vertico
  :straight t
  :custom
  (vertico-mode 1)
  ;; Enable cycling for `vertico-next' and `vertico-previous'.
  (setq vertico-cycle t))

(use-package orderless
  :straight t
  :custom (completion-styles '(orderless)))

(use-package marginalia
  :straight t
  :config
  (setq marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  (marginalia-mode 1))

(use-package savehist
  :straight t
  :config
  (savehist-mode 1))

(use-package consult
  :straight t
  :config
  (global-set-key (kbd "C-s") 'consult-line)
  (define-key minibuffer-local-map (kbd "C-r") 'consult-history))

;;(use-package embark
;;  :straight t
;;  :config
;;  (global-set-key (kbd "C-.") 'embark-act)
;;  (setq prefix-help-command #'embark-prefix-help-command))

(use-package company
  :straight t
  :init
  (global-company-mode))


(use-package dired
  :straight nil
  :bind (("<C-return>" . dired-up-directory))
  :custom ((dired-listing-switches "-alh")))

(with-eval-after-load 'dired
  (define-key dired-mode-map "c" 'find-file))

(use-package dired-launch
  :straight t)
(dired-launch-enable)
;; Somehow these are not bound by default?
(define-key dired-launch-mode-map (kbd "J") 'dired-launch-command)
(define-key dired-launch-mode-map (kbd "K") 'dired-launch-with-prompt-command)
;; Use Sumatra PDF to view PDFs
(setf dired-launch-extensions-map
        (list
         `("pdf" (,pdf-viewer-program))
         ;; open text files with Emacs
         '("txt" (("emacs" find-file)))))

;; Let dired guess the copy/move target based on open dired buffers
(setq dired-dwim-target t)

(use-package dired-single
  :straight t)

(defun my-dired-init ()
  "Bunch of stuff to run for dired, either immediately or when it's
   loaded."
  ;; <add other stuff here>
  (define-key dired-mode-map [remap dired-find-file]
    'dired-single-buffer)
  (define-key dired-mode-map [remap dired-mouse-find-file-other-window]
    'dired-single-buffer-mouse)
  (define-key dired-mode-map [remap dired-up-directory]
    'dired-single-up-directory))

;; if dired's already loaded, then the keymap will be bound
(if (boundp 'dired-mode-map)
    ;; we're good to go; just add our bindings
    (my-dired-init)
  ;; it's not loaded yet, so add our bindings to the load-hook
  (add-hook 'dired-load-hook 'my-dired-init))

(use-package all-the-icons-dired
  :straight t
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package helpful
  :straight t
  :config
  (global-set-key (kbd "C-h f") #'helpful-callable)
  (global-set-key (kbd "C-h v") #'helpful-variable)
  (global-set-key (kbd "C-h k") #'helpful-key)
  (global-set-key (kbd "C-h F") #'helpful-function)
  (global-set-key (kbd "C-h C") #'helpful-command))

(use-package avy
  :straight t
  :config
  (global-set-key (kbd "C-:") 'avy-goto-char)
  (global-set-key (kbd "C-;") 'avy-goto-char-2))

(defun jk/org-mode-setup ()
  (org-indent-mode 1)
  (visual-line-mode 1))

(use-package org
  :straight (:type built-in)
  :hook (org-mode . jk/org-mode-setup)
  :config
  (setq org-hide-emphasis-markers t
        org-pretty-entities t
        org-startup-with-inline-images t
        org-image-actual-width '(300)
        org-auto-align-tags nil
        org-tags-column 0
        org-ellipsis " ▼"
        org-catch-invisible-edits 'show-and-error
        org-special-ctrl-a/e t
        org-insert-heading-respect-content t)

  ;; https://protesilaos.com/codelog/2025-01-16-emacs-org-todo-agenda-basics/
  (setq org-M-RET-may-split-line '((default . nil)))
  (setq org-insert-heading-respect-content t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)

  (setq org-directory "~/org")
  (setq org-agenda-files (list org-directory))
  (setq org-todo-keywords
        '((sequence "TODO(t)" "WAIT(w!)" "|" "CANCEL(c!)" "DONE(d!)")))

  (setq org-clock-sound "~/.emacs.d/Ring10.wav"))


(use-package org-appear
  :straight t
  :hook
  (org-mode . org-appear-mode))


(use-package org-modern
  :straight t
  :config
  (with-eval-after-load 'org (global-org-modern-mode)))


(require 'ox-latex)
(unless (boundp 'org-latex-classes)
  (setq org-latex-classes nil))
(add-to-list 'org-latex-classes
             '("koma-book"
               "\\documentclass[11pt]{scrbook}
                 [NO-DEFAULT-PACKAGES]
                 [PACKAGES]
                 [EXTRA]"
               ("\\part{%s}" . "\\part*{%s}")
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")))
(setq org-latex-listings 'listings)

(use-package ox-reveal
  :straight t)

(use-package org-roam
  :straight t
  :custom
  (org-roam-directory "~/org-roam")
  (org-roam-completion-everywhere t)
  (org-roam-dailies-directory "daily/")
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %<%H:%M %p>: %?"
      :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i" . completion-at-point))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (require 'org-roam-dailies)
  (org-roam-db-autosync-mode))


(use-package multiple-cursors
  :straight t
  :bind (("C-<" . mc/mark-next-like-this)
         ("C->" . mc/mark-previous-like-this)
         ("C-," . mc/unmark-next-like-this))
  )

;; Programming (mainly C++)
;; built-in in Emacs 29, straight not needed
(use-package eglot
  :config
  (add-to-list 'exec-path eglot-exe-path)
  (add-hook 'c-mode-hook 'eglot-ensure)
  (add-hook 'c++-mode-hook 'eglot-ensure)
  (put 'narrow-to-region 'disabled nil)
  (add-hook 'after-init-hook 'global-company-mode)
  :custom
  (eglot-autoshutdown t))

(use-package modern-cpp-font-lock
  :straight t
  :config
  (modern-c++-font-lock-global-mode t))

;; LaTeX and Friends
;; AUCTeX must come after eglot (why?)
(use-package latex
  :straight auctex
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil))


;; Ispell and Hunspell
(use-package ispell
  :straight t
  :config
  (setenv "DICPATH"
          (concat (getenv "HOME") "\\dict"))
  (setenv "LANG" "de_DE")
  (setq ispell-program-name hunspell-program)
  (setq ispell-dictionary "de_DE")
  (setq ispell-local-dictionary "de_DE")
  (setq ispell-local-dictionary-alist
        '(("de_DE" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "de_DE") nil utf-8)))
  (ispell-set-spellchecker-params)
  (setq ispell-hunspell-dictionary-alist ispell-local-dictionary-alist)
  (setq ispell-personal-dictionary "~/.hunspell_personal")
  (global-set-key (kbd "C-c v") #'flyspell-goto-next-error))


(unless (file-exists-p ispell-personal-dictionary)
                       (write-region "" nil ispell-personal-dictionary nil 0))

(use-package yasnippet
  :straight t
  :config
  (yas-reload-all)
  (add-hook 'LaTeX-mode-hook #'yas-minor-mode))


(add-to-list 'load-path "~/.emacs.d/writegood")
(require 'writegood-mode)
(global-set-key "\C-cg" 'writegood-mode)

(use-package magit
  :straight t
  :commands
  (magit-status magit-get-current-branch))

(use-package yaml-mode
  :straight (yaml-mode :type git :host github :repo "yoshiki/yaml-mode")
  :config
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode)))

(setq gc-cons-threshold (* 20 1000 1000))



;;(use-package cmake-mode
;;  :straight t)

(use-package paredit
  :straight t)

;; Common Lisp
;; (load (expand-file-name "~/quicklisp/slime-helper.el"))
;; (setq inferior-lisp-program "sbcl.exe")
;; (slime-setup '(slime-fancy slime-asdf)) ;; and possibly other SLIME extensions
;; (add-hook 'slime-mode-hook
;;           #'(lambda ()
;;              (set-variable lisp-indent-function 'common-lisp-indent-function)
;;              (define-key slime-mode-map [tab] 'slime-indent-and-complete-symbol)
;;              (define-key slime-mode-map (kbd "C-<tab>") 'slime-complete-symbol)))

;; Scheme
;;(use-package geiser-chez
;;  :straight t)

;; Setup bash
;; D:/msys64/msys2_shell.cmd -defterm -here -no-start -mingw64
;;(setq explicit-shell-file-name "D:/msys64/usr/bin/bash.exe")
;;(setq shell-file-name "bash")
;;(setq explicit-bash.exe-args '("--login" "-i"))
;;(setenv "SHELL" shell-file-name)
;;(add-hook 'comint-output-filter-functions 'comint-strip-ctrl-m)

;; Setup zsh
(setq explicit-shell-file-name zsh-programm)
(setq explicit-zsh.exe-args '("--login" "--interactive"))
(setenv "SHELL" shell-file-name)
(add-hook 'comint-output-filter-functions 'comint-strip-ctrl-m)
